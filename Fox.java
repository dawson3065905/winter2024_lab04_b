public class Fox {

	//fields
    private String species;
    private String habitat;
    private String sex;

	//constructor
    Fox(String species, String habitat, String sex) {
        this.species = species;
        this.habitat = habitat;
        this.sex = sex;
    }
	
	//getters
	
	public String getSpecies(){
		return this.species;
	}
	
	public String getHabitat(){
		return this.habitat;
	}
	
	public String getSex(){
		return this.sex;
	}
	
	//setters

	public void setHabitat(String habitat){
		this.habitat = habitat;
	}

	//functions
    public void whatEats() {
        System.out.println("Foxes usually eat small mammals, insects, berries, birds carrion, marine invertebrates, sea birds and fish.");
    }

    public void Sound() {
        if (species.equals("arctic"))
            System.out.println("Barking yowl");
        else
            System.out.println("shriek/scream");
    }


    

}