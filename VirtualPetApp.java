import java.util.Scanner;

public class VirtualPetApp {
    public static void main (String[] args) {
        Fox[] skulk = new Fox[4];
		
		Scanner input = new Scanner(System.in);
		
        for (int i = 0; i < skulk.length; i++) { 
            
            System.out.print("What is this fox's species?: ");
            String species = input.nextLine();

            System.out.print("What is their habitat?: ");
            String habitat = input.nextLine();

            System.out.print("Are they male or female?: ");
            String sex = input.nextLine();
			
			//calls constructor
            skulk[i] = new Fox(species, habitat, sex);
			
			//flavour text
            if (i < skulk.length - 1)
                System.out.println("\nNext fox:");
        }

		//before
		System.out.println("\n" + "Last fox before" + "\n" + "Your fourth fox: " + skulk[skulk.length-1].getSpecies() + ", " + skulk[skulk.length-1].getHabitat() + ", " + skulk[skulk.length-1].getSex() + "\n");
		
		//step 12
		System.out.print("Change habitat of last fox to: ");
        String newHabitat = input.nextLine();
		skulk[skulk.length-1].setHabitat(newHabitat);
		
		//after
		//note, I realised that the following line of code already prints the values of the last animal
		//so I just refactored it to use .length instead of magic number
        System.out.println("\n" + "Last fox after" + "\n" + "Your fourth fox: " + skulk[skulk.length-1].getSpecies() + ", " + skulk[skulk.length-1].getHabitat() + ", " + skulk[skulk.length-1].getSex() + "\n");
		
        skulk[0].Sound();
        skulk[0].whatEats();
		
		input.close();
    }
}
